FROM debian:buster-slim

RUN mkdir /usr/share/man/man1 -p

RUN set -eux; \
        apt-get update; \
        apt-get install -y --no-install-recommends \
                procps wget openjdk-11-jre-headless telnet kafkacat

ARG KAFKA_VERSION=2.5.0
ARG SCALA_VERSION=2.12
ARG KAFKA_ASSET_URL="http://apache.crihan.fr/dist/kafka"

ARG KAFKACONNECT_VERSION=1.2.0
ARG KAFKACONNECT_ASSET_URL=https://github.com/splunk/kafka-connect-splunk/releases/download

ENV KAFKACONNECT_DOWNLOAD_URL=${KAFKACONNECT_ASSET_URL}/v${KAFKACONNECT_VERSION}/splunk-kafka-connect-v${KAFKACONNECT_VERSION}.jar
ENV KAFKA_DOWNLOAD_URL=${KAFKA_ASSET_URL}/${KAFKA_VERSION}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz

RUN mkdir -p /opt/kafkaConnectSplunk/connectors
RUN mkdir -p /opt/src

RUN wget ${KAFKA_DOWNLOAD_URL} -O /opt/kafka.tgz
RUN tar -xf /opt/kafka.tgz -C /opt/kafkaConnectSplunk --strip-components=1
RUN rm /opt/kafka.tgz

RUN wget ${KAFKACONNECT_DOWNLOAD_URL} -O /opt/kafkaConnectSplunk/connectors/splunk-kafka-connect-v${KAFKACONNECT_VERSION}.jar
COPY build/kafka-connect/config/log4j.properties /opt/kafkaConnectSplunk/config/log4j.properties
RUN mkdir -p /var/log/kafkaConnectSplunk/

COPY build/kafka-connect/config/connect.properties /opt/kafkaConnectSplunk/config/connect.properties
RUN ln -snf /usr/share/zoneinfo/Europe/Paris /etc/localtime && echo Europe/Paris > /etc/timezone

ENV KAFKA_HOME=/opt/kafkaConnectSplunk

COPY ./build/scripts/start.sh /opt/kafkaConnectSplunk/start.sh

WORKDIR /opt/kafkaConnectSplunk

VOLUME ["/var/log/kafkaConnect"]
EXPOSE 8083

CMD ["/opt/kafkaConnectSplunk/start.sh"]
