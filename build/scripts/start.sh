#!/bin/bash
export KAFKA_LOG4J_OPTS="-Dlog4j.configuration=file:///opt/kafkaConnectSplunk/config/log4j.properties"
sed -i "s/BOOTSTRAP_SERVER/$BOOTSTRAP_SERVER/g" /opt/kafkaConnectSplunk/config/connect.properties
sed -i "s/GROUP_ID/$GROUP_ID/g" /opt/kafkaConnectSplunk/config/connect.properties 
sed -i "s/TOPIC_CONFIG_PREFIX/$TOPIC_CONFIG_PREFIX/g" /opt/kafkaConnectSplunk/config/connect.properties
/opt/kafkaConnectSplunk/bin/connect-distributed.sh /opt/kafkaConnectSplunk/config/connect.properties
