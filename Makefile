cnf ?= .env
include $(cnf)

help:           ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

.PHONY: build
build:          ## Build the container
	sudo docker build -t $(DOCKER_REPO)/$(APP_NAME) .

build-nc:       ## Build the container without caching
	sudo docker build --no-cache -t $(DOCKER_REPO)/$(APP_NAME) .

run up:         ## Run container 
	sudo docker-compose up --force-recreate

daemon:         ## Run container in daemon mode
	sudo docker-compose up -d --force-recreate

restart:	## Restart the daemon
	sudo docker-compose down
	sudo docker-compose up -d --force-recreate
log:		## Log tail
	sudo docker logs -f $(CONTAINER_NAME)

stop:           ## Stop container
	sudo docker-compose down

shell bash:     ## Launch shell in container
	echo "Lauching shell for $(CONTAINER_NAME)"
	sudo docker exec -it $(CONTAINER_NAME) bash


